gg.setVisible(true)
gg.setVisible(false)

-- ArcMods de War Robots --

function Toast(Toast, Sleep, Exit)
    if gg.isVisible() then
        gg.setVisible(false)
    end
    if Toast ~= nil then
        gg.toast(Toast, true)
    end
    if Sleep then
        gg.sleep(1200)
    end
    if Exit then
        os.exit(gg.setVisible(true))
    end
end

function ObterPonteiro(Nome)
    if SalvarPonteiro == nil then
        SalvarPonteiro = {}
    end

    if Nome == nil and #Nome < 2 then
        Toast("Ponteiro inválido", true)
        return
    end

    if SalvarPonteiro[Nome] ~= nil then
        Toast("Esse ponteiro já foi salvo", true)
        return
    end

    local Encontrado = false

    local Metadata = gg.getRangesList("/global-metadata.dat")

    if #Metadata == 0 then
        Toast("Metadata não encontrada", true)
        return
    end

    gg.clearResults()
    gg.setRanges(gg.REGION_OTHER | gg.REGION_C_ALLOC)
    gg.searchNumber("00;" .. table.concat(gg.bytes(Nome, "UTF-8"), ";") .. ";00::" .. #Nome + 2, 1, false, 536870912, Metadata[1]["start"], Metadata[1]["end"])

    if gg.getResultsCount() == 0 then
        Toast("Não encontrada", true)
        return
    end

    if #Nome + 2 ~= gg.getResultsCount() then
        Toast("Multiplus resultado", true)
        return
    end

    local Ponteiro = string.format("%Xh", gg.getResults(2)[2].address)
    gg.clearResults()
    gg.setRanges(gg.REGION_C_ALLOC)
    gg.searchNumber(Ponteiro, gg.TYPE_DWORD)

    Ponteiro = nil

    for Pos, V in ipairs(gg.getResults(1000), gg.clearResults()) do
        Valor = gg.getValues({ { address = V.address - 8, flags = 16 } })
        if Valor[1].value ~= 0 then
            if Encontrado then
                Toast("Múltiplas instância", true)
                return
            end
            Ponteiro = string.format("%Xh", Valor[1].address)
            Encontrado = true
        end
    end

    SalvarPonteiro[Nome] = Ponteiro
end

function Ponteiro(Nome)
    if SalvarPonteiro[Nome] == nil then
        Toast("Esse ponteiro não existe", true)
        PonteiraAtual = nil
        return
    end

    gg.clearResults()
    gg.setRanges(gg.REGION_ANONYMOUS)
    gg.searchNumber(SalvarPonteiro[Nome], gg.TYPE_DWORD)

    if gg.getResultsCount() == 0 then
        Toast("Não encontrada", true)
        return
    end

    PonteiraAtual = gg.getResults(1000)
    gg.clearResults()
end

function Deslocamento(Offset, Editar)
    if tonumber(Offset) == nil then
        Toast("Offset inválido ", true)
        return
    end
    if PonteiraAtual then
        for Pos, V in ipairs(PonteiraAtual) do
            gg.setValues({ { address = V.address + Offset, flags = 16, value = Editar .. "h" } })
        end
    else
    end
end

ObterPonteiro("MechStats")

gg.toast("Online")
gg.setVisible(false)
gg.alert([[

WR 8.7.0

				🔥Script By Arcyless 🔥



➤ https://discord.gg/JQMyUcFc


]], "➡️ ABRIR")
gg.alert("⚠️ A V I S O  I M P O R T A N T E:\n\n➤ Use War Robots 32 Bits")
gg.setVisible(true)
function START(...)
    menuwar = gg.multiChoice({
        "Habilidad Infinita",
        "Pular Habilidad",
        "Speed Robot V1",
        "Speed Modulo",
        "Speed Mercury",
        "Infinity Mercury",
        "Fechar Script"
    }, nil, "⚡ Script : 32 Bits📍")
    if menuwar == nil then
    else
        if menuwar[1] == true then
            nose()
        end
        if menuwar[2] == true then
            Pular()
        end
        if menuwar[3] == true then
            speed()
        end
        if menuwar[4] == true then
            Modulo()
        end
        if menuwar[5] == true then
            Modulo2()
        end
        if menuwar[6] == true then
            Modulo3()
        end
        if menuwar[7] == true then
            exit()
        end
    end
    XGCK = -1
end

function nose()
    Ponteiro("MechStats")
    Deslocamento(0x24, "00000000")
    Toast("Speed habilidad")
end

function Pular()
    Ponteiro("MechStats")
    Deslocamento(0x2C, "00000000")
    Toast("Pular habilidad")
end

function speed()
    Ponteiro("MechStats")
    Deslocamento(0x14, "00002041")
    Toast("Speed Robot")
end

function Modulo()
    Ponteiro("MechStats")
    Deslocamento(0x28, "00000000")
    Toast("Speed Modulo")
end

function Modulo2()
    Ponteiro("MechStats")
    Deslocamento(0x28, "00000000")
    Toast("Speed Modulo")
end

function Modulo3()
    Ponteiro("MechStats")
    Deslocamento(0x28, "00000000")
    Toast("Speed Modulo")
end

function exit(...)
    gg.toast("Z	")
    print("┌─┐  .─┐☆War Robots ~ 8.7.0 ★")
    print("│▒│ /▒/")
    print("│▒│/▒/★ ")
    print("│▒/▒/─┬─┐")
    print("│▒│▒|▒│▒│★ Empresa:")
    print("┌┴─┴─┐-┘─┘ Arcyless")
    print("│▒┌──┘▒▒▒│")
    print("└┐▒▒▒▒▒▒┌┘★ : ")
    print("└┐▒▒▒▒┌┘")
    os.exit()
end

while true do
    if gg.isVisible(true) then
        XGCK = 1
        gg.setVisible(false)
    end
    if XGCK == 1 then
        START()
    end
end