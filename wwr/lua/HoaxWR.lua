print('Made by OliverXH\n')

local game_x64 = false
local game_libil2cpp = nil

local g_flags = gg.TYPE_DWORD
local g_prefix = '~A '
local g_size_ptr = 0x04

local targetInfo = gg.getTargetInfo()

-- Get system architecture bits
if targetInfo ~= nil then
    game_x64 = targetInfo['x64']
    if game_x64 then
        g_flags = gg.TYPE_QWORD
        g_prefix = '~A8 '
        g_size_ptr = 0x08
    end
else
    gg.toast('Error: Failed to get target info')
    os.exit()
end

local rangesList = gg.getRangesList('libil2cpp.so')
if rangesList == nil then
    gg.toast('Error: Failed to find libil2cpp.so')
    os.exit()
end

local i = 1
repeat
    if rangesList[i].state == 'Xa' then
        game_libil2cpp = rangesList[i].start
    else
        i = i + 1
    end
until game_libil2cpp ~= nil

if game_libil2cpp ~= nil then
    gg.toast('libil2cpp.so: ' .. string.format('0x%X', game_libil2cpp))
else
    gg.toast('Error: Failed to find libil2cpp.so')
    os.exit()
end

local RET = {
    x32 = 'BX LR',
    x64 = 'RET'
}

local data = {
    {
        name = 'AbilityStateWithCooldownAndCharges.get_Cooldown',
        offset = 0x2844678,
        x32 = {
            'MOV R0, #0',
            RET.x32
        },
        x64 = {
            'MOVI D0, #0',
            RET.x64 
             
            
        }
    },
    {
        name = 'AggregateModuleBattleBalance.get_Cooldown',
        offset = 0x29004C8,
        x32 = {
            'MOV R0, #0',
            RET.x32
        },
        x64 = {
            'MOVI D0, #0',
            RET.x64
            
        }
    },
    {
        name = 'MechStats.get_Ability1Cooldown',
        offset = 0x2798084,
        x32 = {
            'MOV R0, #0',
            RET.x32
        },
        x64 = {
            'MOVI D0, #0',
            RET.x64
        }
    },
    {
        name = 'MechStats.get_Ability2Cooldown',
        offset = 0x2798144,
        x32 = {
            'MOV R0, #0',
            RET.x32
        },
        x64 = {
            'MOVI D0, #0',
            RET.x64
        }
    }
}

local tempTable = {}
for i, elem in ipairs(data) do
    local armCode = elem.x32
    if game_x64 then
        armCode = elem.x64
    end
    for j, inst in ipairs(armCode) do
        tempTable[#tempTable + 1] = {
            address = game_libil2cpp + elem.offset + g_size_ptr * (j - 1),
            flags = g_flags,
            
            value = g_prefix .. inst
        }
        if j <= 1 and elem.name ~= nil then
            tempTable[#tempTable].name = elem.name
            gg.addListItems({ tempTable[#tempTable] })
        end
    end
end

gg.setValues(tempTable)

print('Program completed\n')

print('Please test in the game')
