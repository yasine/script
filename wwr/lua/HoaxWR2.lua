-- Made by OliverXH

local game_x64 = false
local game_libil2cpp = nil

local g_flags = gg.TYPE_DWORD
local g_prefix = '~A '
local g_size_ptr = 0x04

local targetInfo = gg.getTargetInfo()

-- Get system architecture bits
if targetInfo ~= nil then
    game_x64 = targetInfo['x64']
    if game_x64 then
        -- g_flags = gg.TYPE_QWORD
        g_prefix = '~A8 '
        -- g_size_ptr = 0x08
    end
else
    print('Error: Failed to get target info')
    os.exit()
end

-- Find libil2cpp.so
local rangesList = gg.getRangesList('libil2cpp.so')
if rangesList == nil then
    print('Error: Failed to get ranges list of libil2cpp.so')
    os.exit()
end

local i = 1
repeat
    if rangesList[i].state == 'Xa' then
        game_libil2cpp = rangesList[i].start
    else
        i = i + 1
    end
until game_libil2cpp ~= nil

if game_libil2cpp ~= nil then
    gg.toast('libil2cpp.so: ' .. string.format('0x%X', game_libil2cpp))
else
    print('Error: Failed to find libil2cpp.so')
    os.exit()
end

gg.addListItems({
    {
        name = 'libil2cpp.so base address',
        address = game_libil2cpp,
        flags = g_flags,
    }
})

-- data
local data = {
    {
        name = 'EquipmentFullInfo.GetReloadTime',
        x64 = {
            offset = 0x248091C,
            instruct = {
                'MOV X0, #100',
                'RET'
            }
        },
        name = 'EquipmentStats.ShotInterval',
        x64 = {
            offset = 0x2751B58,
            instruct = {
                'MOV X0, #0',
                'RET'
            }
        }
    }
}

local tempTable = {}
for i, elem in ipairs(data) do
    local code = elem.x32
    if game_x64 then
        code = elem.x64
    end
    gg.addListItems({
        {
            name = elem.name,
            address = game_libil2cpp + code.offset,
            flags = g_flags,
        }
    })
    if code.instruct ~= nil and #code.instruct > 0 then
        for j, inst in ipairs(code.instruct) do
            tempTable[#tempTable + 1] = {
                address = game_libil2cpp + code.offset + g_size_ptr * (j - 1),
                flags = g_flags,
                value = g_prefix .. inst
            }
        end
    end
end

gg.setValues(tempTable)

print('Program completed\n')

print('Please test in the game')
