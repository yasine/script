userMode = 2 -- 1 = Developer mode, 2 = User mode

GameVersion = "9.9.9"

Script = gg.getTargetInfo()
if Script.versionName == GameVersion then
    goto FIRSTALERT
elseif Script.versionName > GameVersion then
    gg.alert("Your War Robots is outdated")
    os.exit()
elseif Script.versionName < GameVersion then
    gg.alert("Script Version is outdated")
    os.exit()
end

:: FIRSTALERT ::
local FirstAlert = gg.alert("⚠️You have to Update the Offsets by Yourself after every Update, Otherwise the Hacks will not Work Anymore⚠️", "I Understand", "", "Info: Read the Text")
if FirstAlert == "1" then
    gg.toast("Starting")
    goto SECONDALERT
elseif FirstAlert == "2" or "3" then
    gg.toast('⚠️Read the Text First and then click on "I Understand"⚠️')
    goto FIRSTALERT
end
:: SECONDALERT ::
local X = gg.alert('⚠️Please do not Share this Script with anyone⚠️', 'Ok', '', 'Info: Read the Text and click on "Ok"')
if X == "1" then
    gg.toast("Hi And Welcome")
    gg.sleep(300)
    goto HOME
elseif X == "2" or "3" then
    gg.toast('⚠️You have to read the Text and then click on "Ok" to Start the Script⚠️')
    goto SECONDALERT
end

:: HOME ::
function mainMenu()
    MainMenu = gg.multiChoice({
        AbilityDuration .. "",
        AbilityCooldown .. "",
        Speedhack .. "",
        "Exit"
    }, nil, "Script by Frosttok Hacks WR")

    if MainMenu == nil then
    else
        if MainMenu[1] then
            AbilityDurationFunction()
        end
        if MainMenu[2] then
            AbilityCooldownFunction()
        end
        if MainMenu[3] then
            SpeedhackFunction()
        end
        if MainMenu[4] then
            os.exit()
        end
    end
end

AbilityDuration = "No Ability Duration 『✗』"
function AbilityDurationFunction()
    if AbilityDuration == "No Ability Duration 『✗』" then
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x27982C4      --get_Ability1Duration
        FrosttokHacksWR[1].value = "~A BX LR"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x2798384     --get_Ability2Duration
        FrosttokHacksWR[1].value = "~A BX LR"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        gg.toast("No Ability Duration is Enabled")
        AbilityDuration = "No Ability Duration 『✓』"
    elseif AbilityDuration == "No Ability Duration 『✓』" then
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x27982C4   --get_Ability1Duration
        FrosttokHacksWR[1].value = "~A MOV	 X0, #0"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x2798384   --get_Ability2Duration
        FrosttokHacksWR[1].value = "~A MOV	 X0, #0"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        gg.toast("No Ability Duration is Disabled")
        AbilityDuration = "No Ability Duration 『✗』"
    end
end

AbilityCooldown = "No Ability Cooldown 『✗』"
function AbilityCooldownFunction()
    if AbilityCooldown == "No Ability Cooldown 『✗』" then
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x2798084   --get_Ability1Cooldown
        FrosttokHacksWR[1].value = "~A BX LR"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x2798144 --get_Ability2Cooldown
        FrosttokHacksWR[1].value = "~A BX LR"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        gg.toast("No Ability Cooldown is Enabled")
        AbilityCooldown = "No Ability Cooldown 『✓』"
    elseif AbilityCooldown == "No Ability Cooldown 『✓』" then
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x2798084 --get_Ability1Cooldown
        FrosttokHacksWR[1].value = "~A MOV	 X0, #0"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[1].address = LibStart + 0x2798144 --get_Ability2Cooldown
        FrosttokHacksWR[1].value = "~A MOV	 X0, #0"
        FrosttokHacksWR[1].flags = 4
        gg.setValues(FrosttokHacksWR)
        gg.toast("No Ability Cooldown is Disabled")
        AbilityCooldown = "No Ability Cooldown 『✗』"
    end
end

Speedhack = "Speedhack 『✗』"
function SpeedhackFunction()
    if Speedhack == "Speedhack 『✗』" then
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[2] = {}
        FrosttokHacksWR[1].address = LibStart + 0x1C02308
        FrosttokHacksWR[1].value = "hEF0E44E3"
        FrosttokHacksWR[1].flags = 4
        FrosttokHacksWR[2].address = LibStart + (0x1C02308 + 0x4)
        FrosttokHacksWR[2].value = "h1EFF2FE1"
        FrosttokHacksWR[2].flags = 4
        gg.setValues(FrosttokHacksWR)
        gg.toast("Speedhack is Enabeld")
        Speedhack = "Speedhack 『✓』"
    elseif Speedhack == "Speedhack 『✓』" then
        LibStart = gg.getRangesList('libil2cpp.so')[1].start
        FrosttokHacksWR = nil
        FrosttokHacksWR = {}
        FrosttokHacksWR[1] = {}
        FrosttokHacksWR[2] = {}
        FrosttokHacksWR[1].address = LibStart + 0x1C02308
        FrosttokHacksWR[1].value = "-382,908,368"
        FrosttokHacksWR[1].flags = 4
        FrosttokHacksWR[2].address = LibStart + (0x1C02308 + 0x4)
        FrosttokHacksWR[2].value = "-442,544,036"
        FrosttokHacksWR[2].flags = 4
        gg.setValues(FrosttokHacksWR)
        gg.toast("Speedhack is Disabled")
        Speedhack = "Speedhack 『✗』"
    end
end

function valueFromClass(class, offset, tryHard, bit32, valueType)
    Get_user_input = {}
    Get_user_input[1] = class
    Get_user_input[2] = offset
    Get_user_input[3] = tryHard
    Get_user_input[4] = bit32
    Get_user_type = valueType
    start()
end
function loopCheck()
    if userMode == 1 then
        UI()
    elseif error == 3 then
        stopClose()
    end
end
function found_(message)
    if error == 1 then
        found2(message)
    elseif error == 2 then
        found3(message)
    elseif error == 3 then
        found4(message)
    else
        found(message)
    end
end
function found(message)
    if count == 0 then
        gg.clearResults()
        gg.clearList()
        first_error = message
        error = 1
        second_start()
    end
end
function found2(message)
    if count == 0 then
        gg.clearResults()
        gg.clearList()
        second_error = message
        error = 2
        third_start()
    end
end
function found3(message)
    if count == 0 then
        gg.clearResults()
        gg.clearList()
        third_error = message
        error = 3
        fourth_start()
    end
end
function found4(message)
    if count == 0 then
        gg.clearResults()
        gg.clearList()
        gg.alert("❌Value NOT FOUND❌\nError Log:\nTry 1: " .. first_error .. "\nTry 2: " .. second_error .. "\nTry 3: " .. third_error .. "\nTry 4: " .. message .. "\n\nℹ️Try Thisℹ️\n\n🟢1: Make Sure You are Using Correct Version Of game and the dump. 64 bit game and 64 bit dump or 32 bit game 32 bit dump + check try for 32 bit option in script or Version of game and same version of dump.\n\n🟡2: Is the value allocated? if you are searching for player health maybe that value only loads after you load match and took some damage. So try again while playing\n\n🔴3: Is the class name correct? for example if the class is playerscript then don't forget to make P and S capital. So type PlayerScript not playerscript\n\n⚫4: Is the offset right? you have to type 0x before the offset for example the offset is C1 then you have to Give 0xC1 to this script\n\n")
        gg.setVisible(true)
        loopCheck()
    end
end
function user_input_taker()
    :: stort ::
    gg.clearResults()
    if userMode == 1 then
        if Get_user_input == nil then
            default1 = ""
            default2 = ""
            default3 = false
            default4 = false
        else
            default1 = Get_user_input[1]
            default2 = Get_user_input[2]
            default3 = Get_user_input[3]
            default4 = Get_user_input[4]
        end
        Get_user_input = gg.prompt(
                { "\n\nClass Name: ", "Offset: ", "Try Harder --(decreases accuracy)", "Try For 32 bit" },
                { default1, default2, default3, default4 },
                { "text", "text", "checkbox", "checkbox" })
        if Get_user_input ~= nil then
            if (Get_user_input[1] == "") or (Get_user_input[2] == "") then
                gg.alert("ℹ️ Don't Leave Input Blankℹ️")
                goto stort
            end
        else
            gg.alert("ℹ️ Error : Try again ℹ️")
            goto stort
        end
        Get_user_type = gg.choice({ "1. Byte / Boolean", "2. Dword / 32 bit Int", "3. Qword / 64 bit Int", "4. Float", "5. Double" }, nil, "\n\nℹ️ Choose The Output Type ℹ️")
        if Get_user_type == 1 then
            Get_user_type = gg.TYPE_BYTE
        elseif Get_user_type == 2 then
            Get_user_type = gg.TYPE_DWORD
        elseif Get_user_type == 3 then
            Get_user_type = gg.TYPE_QWORD
        elseif Get_user_type == 4 then
            Get_user_type = gg.TYPE_FLOAT
        elseif Get_user_type == 5 then
            Get_user_type = gg.TYPE_DOUBLE
        end
        if Get_user_type ~= gg.TYPE_BYTE then
            if (Get_user_input[2] % 4) ~= 0 then
                gg.alert("ℹ️Hex Offset Must Be An Multiple OF 4ℹ️")
                goto stort
            end
        end
    end
    error = 0
end
function O_initial_search()
    gg.setVisible(false)
    user_input = ":" .. Get_user_input[1]
    if Get_user_input[3] then
        offst = 25
    else
        offst = 0
    end
end
function O_dinitial_search()
    if error > 1 then
        gg.setRanges(gg.REGION_C_ALLOC)
    else
        gg.setRanges(gg.REGION_OTHER)
    end
    gg.searchNumber(user_input, gg.TYPE_BYTE)
    count = gg.getResultsCount()
    if count == 0 then
        found_("O_dinitial_search")
        return 0
    end
    Refiner = gg.getResults(1)
    gg.refineNumber(Refiner[1].value, gg.TYPE_BYTE)
    count = gg.getResultsCount()
    if count == 0 then
        found_("O_dinitial_search")
        return 0
    end
    val = gg.getResults(count)
    gg.addListItems(val)
end
function CA_pointer_search()
    gg.clearResults()
    gg.setRanges(gg.REGION_C_ALLOC | gg.REGION_OTHER)
    gg.loadResults(gg.getListItems())
    gg.searchPointer(offst)
    count = gg.getResultsCount()
    if count == 0 then
        found_("CA_pointer_search")
        return 0
    end
    vel = gg.getResults(count)
    gg.clearList()
    gg.addListItems(vel)
end
function CA_apply_offset()
    if Get_user_input[4] then
        tanker = 0xfffffffffffffff8
    else
        tanker = 0xfffffffffffffff0
    end
    local copy = false
    local l = gg.getListItems()
    if not copy then
        gg.removeListItems(l)
    end
    for i, v in ipairs(l) do
        v.address = v.address + tanker
        if copy then
            v.name = v.name .. ' #2'
        end
    end
    gg.addListItems(l)
end

function CA2_apply_offset()
    if Get_user_input[4] then
        tanker = 0xfffffffffffffff8
    else
        tanker = 0xfffffffffffffff0
    end
    local copy = false
    local l = gg.getListItems()
    if not copy then
        gg.removeListItems(l)
    end
    for i, v in ipairs(l) do
        v.address = v.address + tanker
        if copy then
            v.name = v.name .. ' #2'
        end
    end
    gg.addListItems(l)
end
function Q_apply_fix()
    gg.setRanges(gg.REGION_ANONYMOUS)
    gg.loadResults(gg.getListItems())
    gg.clearList()
    count = gg.getResultsCount()
    if count == 0 then
        found_("Q_apply_fix")
        return 0
    end
    yy = gg.getResults(1000)
    gg.clearResults()
    i = 1
    c = 1
    s = {}
    while (i - 1) < count do
        yy[i].address = yy[i].address + 0xb400000000000000
        gg.searchNumber(yy[i].address, gg.TYPE_QWORD)
        cnt = gg.getResultsCount()
        if 0 < cnt then
            bytr = gg.getResults(cnt)
            n = 1
            while (n - 1) < cnt do
                s[c] = {}
                s[c].address = bytr[n].address
                s[c].flags = 32
                n = n + 1
                c = c + 1
            end
        end
        gg.clearResults()
        i = i + 1
    end
    gg.addListItems(s)
end
function A_base_value()
    gg.setRanges(gg.REGION_ANONYMOUS)
    gg.loadResults(gg.getListItems())
    gg.clearList()
    gg.searchPointer(offst)
    count = gg.getResultsCount()
    if count == 0 then
        found_("A_base_value")
        return 0
    end
    tel = gg.getResults(count)
    gg.addListItems(tel)
end
function A_base_accuracy()
    gg.setRanges(gg.REGION_ANONYMOUS | gg.REGION_C_ALLOC)
    gg.loadResults(gg.getListItems())
    gg.clearList()
    gg.searchPointer(offst)
    count = gg.getResultsCount()
    if count == 0 then
        found_("A_base_accuracy")
        return 0
    end
    kol = gg.getResults(count)
    i = 1
    h = {}
    while (i - 1) < count do
        h[i] = {}
        h[i].address = kol[i].value
        h[i].flags = 32
        i = i + 1
    end
    gg.addListItems(h)
end
function A_user_given_offset()
    local old_save_list = gg.getListItems()
    for i, v in ipairs(old_save_list) do
        v.address = v.address + Get_user_input[2]
        v.flags = Get_user_type
    end
    gg.clearResults()
    gg.clearList()
    gg.loadResults(old_save_list)
    count = gg.getResultsCount()
    if count == 0 then
        found_("Q_apply_fix++")
        return 0
    end
    gg.setVisible(true)
end
function start()
    user_input_taker()
    O_initial_search()
    O_dinitial_search()
    if error > 0 then
        return 0
    end
    CA_pointer_search()
    if error > 0 then
        return 0
    end
    CA_apply_offset()
    if error > 0 then
        return 0
    end
    A_base_value()
    if error > 0 then
        return 0
    end
    if offst == 0 then
        A_base_accuracy()
    end
    if error > 0 then
        return 0
    end
    A_user_given_offset()
    if error > 0 then
        return 0
    end
    loopCheck()
    if error > 0 then
        return 0
    end
end
function second_start()
    O_dinitial_search()
    if error > 1 then
        return 0
    end
    CA_pointer_search()
    if error > 1 then
        return 0
    end
    CA_apply_offset()
    if error > 1 then
        return 0
    end
    Q_apply_fix()
    if error > 1 then
        return 0
    end
    if offst == 0 then
        A_base_accuracy()
    end
    if error > 1 then
        return 0
    end
    A_user_given_offset()
    if error > 1 then
        return 0
    end
    loopCheck()
    if error > 1 then
        return 0
    end
end
function third_start()
    O_dinitial_search()
    if error > 2 then
        return 0
    end
    CA_pointer_search()
    if error > 2 then
        return 0
    end
    if offst == 0 then
        CA2_apply_offset()
    end
    if error > 2 then
        return 0
    end
    A_base_value()
    if error > 2 then
        return 0
    end
    if offst == 0 then
        A_base_accuracy()
    end
    if error > 2 then
        return 0
    end
    A_user_given_offset()
    if error > 2 then
        return 0
    end
    loopCheck()
    if error > 2 then
        return 0
    end
end
function fourth_start()
    O_dinitial_search()
    CA_pointer_search()
    CA2_apply_offset()
    Q_apply_fix()
    if offst == 0 then
        A_base_accuracy()
    end
    A_user_given_offset()
    loopCheck()
end
function UI()
    gg.showUiButton()
    while true do
        if gg.isClickedUiButton() then
            start()
        end
    end
end
function stopClose()
    while true do
        mainMenu()
        gg.setVisible(false)
        while gg.isVisible() == false do
        end
    end
end
if userMode == 2 then
    stopClose()
else
    UI()
end