function mainMenu()
    local option = gg.choice({"God Mode", "I SEE ALL ", "IDK", "Exit"}, nil, "Select an option:")
    
    if option == 1 then
        gg.alert('God Mode')
        gg.setRanges(gg.REGION_ANONYMOUS)
        gg.searchNumber('-1D;2,146,435,071D;1.0E;1,072,693,248D;0D;1D;-1D;-1D::37', gg.TYPE_DOUBLE)
        gg.refineNumber('1', gg.TYPE_DOUBLE)
        gg.getResults('1000')
        gg.editAll('7', gg.TYPE_DOUBLE)
        gg.clearResults()
        gg.alert("God Mode activated")
    elseif option == 2 then
        h1() -- I SEE ALL
    elseif option == 3 then
        h2() -- IDK
    elseif option == 4 then
        gg.alert("Exiting script")
        os.exit() -- Exit the script
    elseif option == nil then
        gg.alert("No option selected")
    end
end

function h1()
    gg.alert("I SEE ALL")
    gg.setRanges(gg.REGION_ANONYMOUS)
    gg.searchNumber("0.33000001311F;80.0F:5", gg.TYPE_FLOAT)
    gg.refineNumber("0.33000001311", gg.TYPE_FLOAT)
    gg.getResults("2")
    gg.editAll("350", gg.TYPE_FLOAT)
    gg.toast("Script made by ham")
    gg.clearResults()
end

function h2()
    gg.setRanges(gg.REGION_ANONYMOUS)
    gg.searchNumber("350.0F;80.0F:5", gg.TYPE_FLOAT)
    gg.refineNumber("350", gg.TYPE_FLOAT)
    gg.getResults("2")
    gg.editAll("0.33000001311", gg.TYPE_FLOAT)
    gg.toast("Script made by ham")
    gg.clearResults()
end

while true do
    if gg.isVisible(true) then
        gg.setVisible(false)
        mainMenu() -- Display the main menu
    end
    gg.sleep(100) -- Sleep to avoid excessive CPU usage
end
