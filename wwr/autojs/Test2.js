var targetpngath = "/sdcard/Pictures/Screenshots/"
if (!requestScreenCapture(1)) {
    toast("请求截图失败");
    exit();
}

var screenshots = $images.read(targetpngath + "Screenshot_20220929-004143.png");
// 在该图片中找色，指定找色区域为在位置(400, 500)的宽为300长为200的区域，指定找色临界值为4
var  point = findColor(screenshots, "#9FF8FE", {
    region: [50, 1000, 30, 30],
    threshold: 2
});
screenshots.recycle();
if(point){
    console.log("找到啦:" + point);
    click(50, 1000);
}else{
   toast("没找到");
}