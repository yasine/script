//auto.waitFor()
//var appName = "War Robots";
//launchApp(appName);
//sleep(10000);
// 请求截图
var pngpath = "/sdcard/wwr/";
var appName = "War Robots";
var targetpngath = "/sdcard/Pictures/Screenshots/";
if (!requestScreenCapture(1)) {
    toast("请求截图失败");
    exit();
}


//ClickButton("main.png", "#FF6C2F", 50, 50);
//toast("前往战斗");
var point1 = null;
var point2 = null;
var point3 = null;
var point4 = null;
var point5 = null;
var point6 = null;
var point7 = null;
var point8 = null;
var point9 = null;
var point10 = null;
var point11 = null;
var t = 0;
var s = 1;
while (true) {
    sleep(1000);
    t++;
    toastLog("等待" + t + "秒");
    if (t > 500) {
        //launchApp(appName);
        //关闭应用
        //sleep(10 * 1000);
        //killApp();
        //launchApp(appName);
        //sleep(30000);
    }
    captureScreen(pngpath + "main.png");
    var screenshots = $images.read(pngpath + "main.png");
    //var screenshots = $images.read(targetpngath + "Screenshot_20221025-230228.png");
    point1 = findColor(screenshots, "#E932FF", {
        region: [850, 240, 30, 30],
        threshold: 2
    });
    point2 = findColor(screenshots, "#F1FCF8", {
        region: [1710, 30, 30, 30],
        threshold: 2
    });
    point3 = findColor(screenshots, "#FFFFFF", {
        region: [1809, 1021, 30, 30],
        threshold: 2
    });
    point4 = findColor(screenshots, "#934AFA", {
        region: [900, 670, 30, 30],
        threshold: 2
    });
    point5 = findColor(screenshots, "#00BB88", {
        region: [1250, 1000, 10, 10],
        threshold: 2
    });
    point6 = findColor(screenshots, "#FF6C2F", {
        region: [1600, 1000, 30, 30],
        threshold: 2
    });
    point7 = findColor(screenshots, "#2A536B", {
        region: [800, 400, 30, 30],
        threshold: 2
    });
    point8 = findColor(screenshots, "#FCC050", {
        region: [1830, 1000, 30, 30],
        threshold: 2
    });
    point9 = findColor(screenshots, "#ECEADF", {
        region: [90, 660, 30, 30],
        threshold: 2
    });
    point10 = findColor(screenshots, "#9FF8FE", {
        region: [50, 1000, 30, 30],
        threshold: 2
    });
    point11 = findColor(screenshots, "#DE2910", {
        region: [1440, 280, 30, 30],
        threshold: 2
    });

    
    screenshots.recycle();
    //变更机器人倒计时
    if (t == 75) {
        //没被击杀，自动变更机器人
        //右上角菜单
        click(1845, 50);
        sleep(500);
        //变更机器人
        click(950, 400);
    }
    if (point1) {
        //选择机器人界面
        ClickButton("selectrobot.png", "#00FFE9", -50, -50);
    } else if (point3) {
        t = 0;
        //选择无人机界面
        ClickButton("selectfly.png", "#FFA600", 50, 50);
        sleep(500);
        //确认
        click(1270, 720);
        if (s == 1) {
            sleep(5000);
            changeRobaot();
            s = 0;
        }
    } else if (point4) {
        //关闭广告
        click(1540, 350);
    } else if (point5) {
        //关闭行动升级弹窗
        click(1250, 1000);
    } else if (point6) {
        //战斗结算界面
        click(1600,1000);
        s=1;
    } else if (point2) {
        //离开战斗界面
        click(point2.x + 40, point2.y);
        s=1;
    } else if (point7) {
        //不能变更机器人
        click(1270, 280);
    } else if (point8) {
        //主界面
        //前往战斗
        click(1740, 160);
    } else if (point9) {
        ClickButton("selectmodel.png", "#55FFBD", 50, 50);
        toast("选择乱斗模式");
        ClickButton("luandoumodel.png", "#FF6C2F", 50, 50);
        toast("前往战斗");
    }else if (point10) {
       //查找到后退按钮
       click(50, 1000);
    }
    else if (point11) {
        //在设置界面
        click(60, 1000);
     }
}


exit();

function ClickButton(pngname, color, xoffest, yoffset) {
    var point;
    var i = 0;
    while (point == null && i < 10) {
        sleep(1000);
        captureScreen(pngpath + pngname);
        var img = $images.read(pngpath + pngname);
        point = findColor(img, color);
        img.recycle();
        i++;
    }
    if (point != null) {
        toastLog(point);
        click(point.x + xoffest, point.y + yoffset);
        return true;
    }
    return false;
}

function killApp() {
    let packageName = currentPackage();
    app.openAppSetting(packageName);
    text(app.getAppName(packageName)).waitFor();
    sleep(1000);
    click(570, 510);
    sleep(1000);
    click(790, 1090);
}

function changeRobaot() {
    //右上角菜单
    click(1845, 50);
    sleep(500);
    //变更机器人
    click(950, 400);
    sleep(500);
    //选择第二个机器人
    click(670, 740);
    //选择第二个机器人
    sleep(500);
    click(670, 740);
    //确认
    sleep(500);
    click(1270, 720);
}





